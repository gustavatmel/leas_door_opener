#define outLeft 0
#define outRight 1
#define redLed 3
#define fsrPin 1
//int fsrPin = 1;       
int count = 0;    
bool flagLeft = false;
bool flagRight = false;


void setup() {
  pinMode(outLeft, OUTPUT);
  pinMode(outRight, OUTPUT);
  pinMode(redLed, OUTPUT);
}

void loop() {
  while(analogRead(fsrPin) < 500) {
    if (count == 0) {
      digitalWrite(redLed, HIGH);
    }
    else if (count == 10) {
      digitalWrite(redLed, LOW);
    }
    else if (count == 100) {
      digitalWrite(redLed, HIGH); 
    }
    else if (count == 110) {
      digitalWrite(redLed, LOW); 
    }
    else if (count == 120) {
      digitalWrite(redLed, HIGH); 
    }
    else if (count == 130) {
      digitalWrite(redLed, LOW); 
    }
    else if (count == 140) {
      digitalWrite(redLed, LOW); 
    }
    else if (count == 200) {
      digitalWrite(redLed, HIGH); 
    }
    else if (count == 210) {
      digitalWrite(redLed, LOW); 
    }
    else if (count == 220) {
      digitalWrite(redLed, HIGH); 
    }
    else if (count == 230) {
      digitalWrite(redLed, LOW); 
    }
    else if (count == 240) {
      digitalWrite(redLed, HIGH); 
    }
    else if (count == 250) {
      digitalWrite(redLed, LOW); 
    }
    delay(10);
    count++;
  }
  
  digitalWrite(redLed, LOW);

  if (count <= 100 && count != 0){
    flagRight = !flagRight;
    digitalWrite(outRight, flagRight);
  }
  else if (count <= 200 && count != 0) {
    flagLeft = !flagLeft;
    digitalWrite(outLeft, flagLeft);
  }
  else if (count != 0) {
    if (flagRight == HIGH || flagLeft == HIGH){
      digitalWrite(outRight, LOW);
      digitalWrite(outLeft, LOW);
    }
    else if(flagRight == LOW && flagLeft == LOW){
      digitalWrite(outRight, HIGH);
      digitalWrite(outLeft, HIGH);
    }
  }

  count = 0;
}

